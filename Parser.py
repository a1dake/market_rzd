import sys
from datetime import datetime, timedelta
import requests
import cfscrape
import os
import urllib3
currentdir = os.path.dirname(os.path.realpath(__file__))
sys.path.append("/home/manage_report")
from Send_report.mywrapper import magicDB


class Parser:
    def __init__(self, parser_name: str):
        self.session = cfscrape.create_scraper(sess=requests.Session())
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_data()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def get_data(self):
        date_format = '%d.%m.%Y %H:%M'
        today = datetime.now()
        yesterday = today - timedelta(days=1)
        tomorrow = today + timedelta(days=1)
        from_date = yesterday.strftime(date_format)
        to_date = tomorrow.strftime(date_format)
    
        print('from: {}, to: {}'.format(from_date, to_date))

        content = []
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning) 
        session = requests.Session()
        url = 'https://eshoprzd.ru/rest/elsearch/api/v1/searchPurchase'
        session.headers.update({
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36 Edg/107.0.1418.62'
        })
        session.get('https://eshoprzd.ru/commercePurchases', verify=False)

        json_data = {"itemsPerPage":100, "page": 0, "search":"", "state":"home", "notChangeCounter":False, "searchByField":{"type":"rzd223", "indexes":"purchase_rzd223"}, "orderBy":"publicDate", "orderAsc":False}
        response = session.post(url, json=json_data, verify=False)
        data = response.json()

        for item in data['items']:
            date = datetime.fromtimestamp(item.get("publicDate") / 1000)
            if date > yesterday and date < tomorrow:
                item_data = {
                    'type': 2,
                    'title': '',
                    'purchaseNumber': '',
                    'fz': 'Коммерческие',
                    'purchaseType': '',
                    'url': '',
                    'lots': [],
                    'attachments': [],
                    'procedureInfo': {
                                    'endDate': '',
                                    'scoringDate': ''
                                    },

                    'customer': {
                                    'fullName': '',
                                    'factAddress': '',
                                    'inn': 0,
                                    'kpp': 0,
                                },
                    'contactPerson': {
                                    'lastName': '',
                                    'firstName': '',
                                    'middleName': '',
                                    'contactPhone': '',
                                    'contactEMail': '',
                                    }
                }

                item_data['title'] = str(self.get_title(item))
                item_data['purchaseNumber'] = str(self.get_purchase_number(item))
                item_data['purchaseType'] = str(self.get_purchase_type(item))
                item_data['url'] = 'https://eshoprzd.ru/purchases/' + str(item.get('id'))

                item_data['customer']['fullName'] = str(self.get_full_name(item))
                item_data['customer']['inn'] = int(self.get_inn(item))
                item_data['customer']['kpp'] = inn = int(self.get_kpp(item))

                item_data['procedureInfo']['endDate'] = self.get_end_date(item)
                item_data['procedureInfo']['scoringDate'] = self.get_scor_date(item)
                
                last_name, first_name, middle_name = self.get_name(item)
                item_data['contactPerson']['lastName'] = str(last_name)
                item_data['contactPerson']['firstName'] = str(first_name)
                item_data['contactPerson']['middleName'] = str(middle_name)
                item_data['contactPerson']['contactPhone'] = str(self.get_phone(item))
                item_data['contactPerson']['contactEMail'] = str(self.get_email(item))

                lots = self.get_tender_items(item)
                item_data['lots'].append(lots)
                content.append(item_data)
        return content

    def get_title(self, data):
        try:
            title = data.get('name')
        except:
            title = ''
        return title
    
    def get_purchase_type(self, data):
        try:
            type = data.get('type')
        except:
            type = ''
        return type
        
    def get_purchase_number(self, data):
        try:
            number = data.get('number')
        except:
            number = ''
        return number
        
    def get_full_name(self, data):
        try:
            name = data.get('customer')['fullName']
        except:
            name = ''
        return name
        
    def get_inn(self, data):
        try:
            inn = data.get('customer')['inn']
            if not inn or inn is None:
                inn = 0
        except:
            inn = 0
        return inn
        
    def get_kpp(self, data):
        try:
            kpp = data.get('customer')['kpp']
            if not kpp or kpp is None:
                kpp = 0
        except:
            kpp = 0
        return kpp
          
    def get_name(self, data):
        try:
            name = data['contactPerson'].split()
            last_name = name[0]
            first_name = name[1]
            middle_name = name[2]
        except:
            last_name = ''
            first_name = ''
            middle_name = ''
        return last_name, first_name, middle_name
        
    def get_phone(self, data):
        try:
            phone = data.get('customer')['phone']
        except:
            phone = ''
        return phone

    def get_email(self, data):
        try:
            email = data.get('email')
        except:
            email = ''
        return email
        
    def get_end_date(self, data):
        try:
            date_format = '%d.%m.%Y %H:%M'
            formate = datetime.fromtimestamp(data.get('endDate') / 1000)
            date = formate.strftime(date_format)
            formatted_date = self.formate_date(date)
        except:
            formatted_date = None
        return formatted_date
        
    def get_scor_date(self, data):
        try:
            date_format = '%d.%m.%Y %H:%M'
            formate = datetime.fromtimestamp(data.get('deliveryDate') / 1000)
            date = formate.strftime(date_format)
            formatted_date = self.formate_date(date)
        except:
            formatted_date = None
        return formatted_date
        
    def get_tender_items(self, data):
        try:
            lots_data = []
            if len(data['positionPurchase']) > 0:
                lots = {
                    'region': '',
                    'address': '',
                    'price': '',
                    'lotItems': []
                }
                for lot in data['positionPurchase']:
                    names = {
                    'code': '',
                    'name': ''
                    }
                    code = lot.get("seqNumber")
                    if code is None:
                        names['code'] = ''
                    else:
                        names['code'] = str(lot.get("seqNumber"))

                    name = lot.get("name")
                    if name is None:
                        names['name'] = ''
                    else:
                        names['name'] = ' '.join(str(name).split())

                    lots['lotItems'].append(names)
                    lots['price'] = str(lot.get("price")) or ''
                    lots['address'] = ' '.join(str(data['deliveryAddress']).split()) or ''
                    lots['region'] = str(data['deliveryRegionName']) or ''
                lots_data.append(lots)
        except:
            return []
        return lots_data

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%d.%m.%Y %H:%M')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
